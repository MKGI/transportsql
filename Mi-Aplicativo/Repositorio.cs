﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mi_Aplicativo
{
    public class Repositorio
    {
        public List<string> GetProcedure(string ip, string baseDatos, string usuario, string password, string schema, string procedureName)
        {
            string realpocedure = "";
            bool contner = procedureName.Contains("]");
            if (contner == true)
            {
                realpocedure = $"{schema}.{procedureName}";
            }
            else
            {
                realpocedure = $"{schema}.[{procedureName}]";
            }

            var parameters = new DynamicParameters();
            parameters.Add("@objname", realpocedure);
            try
            {
                using (IDbConnection db = new SqlConnection($"Data Source={ip};Initial Catalog={baseDatos};User ID={usuario};Password={password};MultipleActiveResultSets=True"))
                {
                    return db.Query<string>("sp_helpText", parameters, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<string>();
            }
        }
    }
}
