﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Smo.SqlEnum;


namespace Mi_Aplicativo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            BuscarNombreDB();
        }
        public void BuscarNombreDB()
        { 
            string con = $"Data Source={ipTextBox.Text};Initial Catalog={DbComboBox.Text};User ID={usuarioTextBox.Text};Password={passwordTextBox.Text};MultipleActiveResultSets=True";
            Server srv = new Server();

            srv.ConnectionContext.ConnectionString = con;

            try
            {
                DbComboBox.Items.Clear();
                foreach (Database db in srv.Databases)
                {
                    DbComboBox.Items.Add(db.Name);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.ToString());
            }
        }
        private bool CamposCompletados()
        {
            if (string.IsNullOrEmpty(ipTextBox.Text) || string.IsNullOrEmpty(DbComboBox.Text) || string.IsNullOrEmpty(usuarioTextBox.Text)
                || string.IsNullOrEmpty(passwordTextBox.Text) || string.IsNullOrEmpty(grantTextBox.Text))
                return false;

            return true;
        }

        private void procedureButton_Click(object sender, EventArgs e)
        {
            if (!CamposCompletados())
            {
                MessageBox.Show("Se requiere llenar todos los campos.", "Crear Procedures", MessageBoxButtons.OK);
                return;
            }

            List<string> lineList = new List<string>();
            StringBuilder resultText = new StringBuilder();

            var array = objetoTextBox.Text.Split('\n');
            
            Array.ForEach(array, x =>
            {
                string pivot = x.TrimEnd(new char[] { '\r', '\n' });
                if (!string.IsNullOrEmpty(pivot))
                    lineList.Add(pivot);
            });

            resultText.Append($"USE {DbComboBox.Text} \r\nGO\r\n");
            foreach (string line in lineList)
            {
               if (string.IsNullOrEmpty(line))
                    continue;
                string script = "";
                var schema = "";
                var procedureName = "";
                var partes = line.Split(new char[] { '.', ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);

                //if (partes.Count() == 2)
                //{
                    schema = partes.Count() == 1 ? "dbo" : partes.First();
                    procedureName = partes.Last();
                //}
                //if (partes.Count() == 2 && partes[0].ToString() == "dbo")
                //{
                //    schema = partes.Count() == 1 ? "dbo" : partes.First();
                //    procedureName = partes.Last();
                //}
                //if (partes.Count() == 2 && partes[0].ToString() != esquemaTextField.Text && partes[0].ToString() != "RC")
                //{
                //    schema = esquemaTextField.Text;
                //    procedureName = partes.First() + "." + partes.Last();
                //}
                //if (partes.Count() == 2 && partes[0].ToString() == "RC")
                //{
                //    schema = partes.Count() == 1 ? "RC" : partes.First();
                //    procedureName = partes.Last();
                //}
                //if (partes.Count() == 1 )
                //{
                //    schema = esquemaTextField.Text;
                //    procedureName = partes[0].ToString();
                //}
                  
                Repositorio res = new Repositorio();
                var procedureContentList = (new Repositorio()).GetProcedure(ipTextBox.Text, DbComboBox.Text, usuarioTextBox.Text, passwordTextBox.Text, schema, procedureName);
                    if (procedureContentList.Any())
                        script = (new ScriptDB()).GetProcedureScript(procedureContentList, schema , procedureName, grantTextBox.Text);
                    else
                        script = $"--<WARNING>\r\nEL PROCEDIMIENTO {line} NO SE PUDO ENCONTRAR\r\n--</WARNING>";

                    resultText.Append(script);
                
            }

            resultTextBox.Text = resultText.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            objetoTextBox.Text = "";
            resultTextBox.Text = "";
        }

        private void buttonTablas_Click(object sender, EventArgs e)
        {
            if (!CamposCompletados())
            {
                MessageBox.Show("Se requiere llenar todos los campos.", "Crear Procedures", MessageBoxButtons.OK);
                return;
            }

            List<string> lineList = new List<string>();
            StringBuilder resultText = new StringBuilder();

            var array = objetoTextBox.Text.Split('\n');

            Array.ForEach(array, x =>
            {
                string pivot = x.TrimEnd(new char[] { '\r', '\n' });
                if (!string.IsNullOrEmpty(pivot))
                    lineList.Add(pivot);
            });

            resultText.Append($"USE {DbComboBox.Text} \r\nGO\r\n");
            foreach (string line1 in lineList)
            {
                if (string.IsNullOrEmpty(line1))
                    continue;

                var partes = line1.Split('.').ToList();
                string schema = partes.Count() == 1 ? "dbo" : partes.First();
                string tabla = partes.Last();

                string script = (new ScriptDB()).GetTableScript(ipTextBox.Text, DbComboBox.Text, usuarioTextBox.Text, passwordTextBox.Text, schema, tabla);
                
                resultText.Append(script);
            }

            resultTextBox.Text = resultText.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DbComboBox.SelectedIndex = 0;
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void conectar_Click(object sender, EventArgs e)
        {

                String con = $"Data Source={ipTextBox.Text};Initial Catalog={DbComboBox.Text};User ID={usuarioTextBox.Text};Password={passwordTextBox.Text};MultipleActiveResultSets=True";
                Server srv = new Server();

                srv.ConnectionContext.ConnectionString = con;

            try
            {
                DbComboBox.Items.Clear();
                foreach (Database db in srv.Databases)
                {
                    DbComboBox.Items.Add(db.Name);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Desconectado" + Ex.Message);
            }

        }
    }
}
