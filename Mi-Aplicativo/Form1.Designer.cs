﻿namespace Mi_Aplicativo
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.usuarioTextBox = new System.Windows.Forms.TextBox();
            this.ipTextBox = new System.Windows.Forms.TextBox();
            this.objetoTextBox = new System.Windows.Forms.TextBox();
            this.procedureButton = new System.Windows.Forms.Button();
            this.DbComboBox = new System.Windows.Forms.ComboBox();
            this.grantTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonTablas = new System.Windows.Forms.Button();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.conectar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Usuario";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "DB";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "IP";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(11, 120);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(136, 20);
            this.passwordTextBox.TabIndex = 7;
            this.passwordTextBox.Text = "p@$$W0rD";
            this.passwordTextBox.TextChanged += new System.EventHandler(this.passwordTextBox_TextChanged);
            // 
            // usuarioTextBox
            // 
            this.usuarioTextBox.Location = new System.Drawing.Point(11, 78);
            this.usuarioTextBox.Name = "usuarioTextBox";
            this.usuarioTextBox.Size = new System.Drawing.Size(136, 20);
            this.usuarioTextBox.TabIndex = 8;
            this.usuarioTextBox.Text = "mkguser";
            // 
            // ipTextBox
            // 
            this.ipTextBox.Location = new System.Drawing.Point(11, 35);
            this.ipTextBox.Name = "ipTextBox";
            this.ipTextBox.Size = new System.Drawing.Size(136, 20);
            this.ipTextBox.TabIndex = 10;
            this.ipTextBox.Text = "192.168.100.13";
            // 
            // objetoTextBox
            // 
            this.objetoTextBox.Location = new System.Drawing.Point(154, 14);
            this.objetoTextBox.Multiline = true;
            this.objetoTextBox.Name = "objetoTextBox";
            this.objetoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.objetoTextBox.Size = new System.Drawing.Size(357, 525);
            this.objetoTextBox.TabIndex = 6;
            // 
            // procedureButton
            // 
            this.procedureButton.Location = new System.Drawing.Point(10, 318);
            this.procedureButton.Name = "procedureButton";
            this.procedureButton.Size = new System.Drawing.Size(136, 23);
            this.procedureButton.TabIndex = 5;
            this.procedureButton.Text = "Crear Procedures";
            this.procedureButton.UseVisualStyleBackColor = true;
            this.procedureButton.Click += new System.EventHandler(this.procedureButton_Click);
            // 
            // DbComboBox
            // 
            this.DbComboBox.FormattingEnabled = true;
            this.DbComboBox.Location = new System.Drawing.Point(11, 222);
            this.DbComboBox.Name = "DbComboBox";
            this.DbComboBox.Size = new System.Drawing.Size(136, 21);
            this.DbComboBox.TabIndex = 16;
            // 
            // grantTextBox
            // 
            this.grantTextBox.Location = new System.Drawing.Point(12, 266);
            this.grantTextBox.Name = "grantTextBox";
            this.grantTextBox.Size = new System.Drawing.Size(135, 20);
            this.grantTextBox.TabIndex = 18;
            this.grantTextBox.Text = "VBAGMAUSER";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Usuario Grant";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 376);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Limpiar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonTablas
            // 
            this.buttonTablas.Location = new System.Drawing.Point(10, 347);
            this.buttonTablas.Name = "buttonTablas";
            this.buttonTablas.Size = new System.Drawing.Size(136, 23);
            this.buttonTablas.TabIndex = 21;
            this.buttonTablas.Text = "Crear Tablas";
            this.buttonTablas.UseVisualStyleBackColor = true;
            this.buttonTablas.Click += new System.EventHandler(this.buttonTablas_Click);
            // 
            // resultTextBox
            // 
            this.resultTextBox.Location = new System.Drawing.Point(517, 14);
            this.resultTextBox.Multiline = true;
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.resultTextBox.Size = new System.Drawing.Size(576, 525);
            this.resultTextBox.TabIndex = 22;
            // 
            // conectar
            // 
            this.conectar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conectar.Location = new System.Drawing.Point(10, 153);
            this.conectar.Name = "conectar";
            this.conectar.Size = new System.Drawing.Size(138, 23);
            this.conectar.TabIndex = 23;
            this.conectar.Text = "CONECTAR";
            this.conectar.UseVisualStyleBackColor = true;
            this.conectar.Click += new System.EventHandler(this.conectar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 585);
            this.Controls.Add(this.conectar);
            this.Controls.Add(this.resultTextBox);
            this.Controls.Add(this.buttonTablas);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.grantTextBox);
            this.Controls.Add(this.DbComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.usuarioTextBox);
            this.Controls.Add(this.ipTextBox);
            this.Controls.Add(this.objetoTextBox);
            this.Controls.Add(this.procedureButton);
            this.Name = "Form1";
            this.Text = "TRANSPORTS";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox usuarioTextBox;
        private System.Windows.Forms.TextBox ipTextBox;
        private System.Windows.Forms.TextBox objetoTextBox;
        private System.Windows.Forms.Button procedureButton;
        private System.Windows.Forms.ComboBox DbComboBox;
        private System.Windows.Forms.TextBox grantTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonTablas;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.Button conectar;
    }
}

