﻿using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;
using NPoco.Expressions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VisioForge.Shared.AForge.Math.Geometry;
using VisioForge.Shared.MediaFoundation.OPM;

namespace Mi_Aplicativo
{
    public class ScriptDB
    {
        public ScriptDB()
        {
        }

        public string GetProcedureScript(IEnumerable<string> procedureList, string dominio, string procedure, string username)
        {
            bool contener3 = dominio.Contains("dbo");
            bool contener2 = dominio.Contains("RC");
            bool contener = procedure.Contains("[");
            string procedurecorrecto;
            var cadena = "";
            if (contener == true)
            {
                int contar = procedure.Count() - 2;
                procedurecorrecto = procedure.Substring(1, contar);

                string procedureQuery = "";
                string ifExistQuery = "\r\n" + "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[" + dominio + "].[" + procedurecorrecto + "]') AND type in (N'P', N'PC'))\r\n" + "BEGIN" + "\r\n" + "EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [" + dominio + "].[" + procedurecorrecto + "] AS'" + "\r\n" + "END" + "\r\n" + "GO" + "\r\n\r\n";

                /*int total = procedureList.Count() - cantidad.ToString();*/
                foreach (var line in procedureList)
                {
                    cadena = line;
                    if (line.Contains($"CREATE PROCEDURE [{dominio}].[{procedurecorrecto}]\r\n"))
                    {
                        string pattern = @"\bCREATE\b";
                        string replace = "ALTER";
                        cadena = Regex.Replace(line, pattern, replace);
                    }

                    procedureQuery += cadena;
                }
                procedureQuery += $"\r\nGO\r\n\r\n";
                procedureQuery += $"GRANT EXECUTE, VIEW DEFINITION ON {dominio}.{procedurecorrecto } TO {username}\r\n";
                procedureQuery += $"GO\r\n\r\n";
                procedureQuery += "--========================================================";

                return $"{ifExistQuery}{procedureQuery}";
            }
            else if (contener == false && contener2 == true || contener3 == true) {
                string procedureQuery = "";
                string ifExistQuery = "\r\n" + "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[" + dominio + "].[" + procedure + "]') AND type in (N'P', N'PC'))\r\n" + "BEGIN" + "\r\n" + "EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [" + dominio + "].[" + procedure + "] AS'" + "\r\n" + "END" + "\r\n" + "GO" + "\r\n\r\n";

                foreach (var line in procedureList)
                {
                    string pattern = @"\bCREATE\b";
                    string replace = "ALTER";
                    cadena = Regex.Replace(line, pattern, replace);
                    procedureQuery += cadena;
                }
                procedureQuery += $"\r\nGO\r\n\r\n";
                procedureQuery += $"GRANT EXECUTE, VIEW DEFINITION ON {dominio}.{procedure} TO {username}\r\n";
                procedureQuery += $"GO\r\n\r\n";
                procedureQuery += "--========================================================";

                return $"{ifExistQuery}{procedureQuery}";
            }
            else 
            {
                string procedureQuery = "";
                string ifExistQuery = "\r\n" + "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[" + dominio + "].[" + procedure + "]') AND type in (N'P', N'PC'))\r\n" + "BEGIN" + "\r\n" + "EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [" + dominio + "].[" + procedure + "] AS'" + "\r\n" + "END" + "\r\n" + "GO" + "\r\n\r\n";

                foreach (var line in procedureList)
                {
                    cadena = line;
                    if (line.Contains($"CREATE PROCEDURE [{dominio}].[{procedure}]\r\n"))
                    {
                        string pattern = @"\bCREATE\b";
                        string replace = "ALTER";
                        cadena = Regex.Replace(line, pattern, replace);
                    }
                    procedureQuery += cadena;
                }
                procedureQuery += $"\r\nGO\r\n\r\n";
                procedureQuery += $"GRANT EXECUTE, VIEW DEFINITION ON {dominio}.{procedure } TO {username}\r\n";
                procedureQuery += $"GO\r\n\r\n";
                procedureQuery += "--========================================================";

                return $"{ifExistQuery}{procedureQuery}";
            }
        }

        public string GetTableScript(string ip, string dataBaseName, string user, string password, string schema, string table)
        {
            string script = "";

            string con = $"Data Source={ip};Initial Catalog={dataBaseName};User ID={user};Password={password};MultipleActiveResultSets=True";

            Server srv = new Server();
            srv.ConnectionContext.ConnectionString = con;

            Database db = new Database();

            db = srv.Databases[dataBaseName];
            Scripter scrp = new Scripter(srv);
            var urns = new List<Urn>();

            scrp.Options.EnforceScriptingOptions = true;
            scrp.Options.IncludeHeaders = true;
            scrp.Options.SchemaQualify = true;
            scrp.Options.SchemaQualifyForeignKeysReferences = true;
            scrp.Options.NoCollation = true;
            scrp.Options.DriAllConstraints = true;
            scrp.Options.DriAll = true;
            scrp.Options.DriAllKeys = true;
            scrp.Options.DriIndexes = true;
            scrp.Options.ClusteredIndexes = true;
            scrp.Options.NonClusteredIndexes = true;
            scrp.Options.ToFileOnly = true;

            Table tableCollection = db.Tables[table, schema];

            if (tableCollection != null && db.Tables.Count > 0)
            {
                StringCollection result = tableCollection.Script();

                foreach (var line in result)
                    if (!tableCollection.IsSystemObject)
                        urns.Add(tableCollection.Urn);

                // Instanciando un string builder para construir el script
                StringBuilder builder = new StringBuilder();
                System.Collections.Specialized.StringCollection sc = scrp.Script(urns.ToArray());

                foreach (string st in sc)
                {
                    // Agregando los comandos al string builder
                    builder.AppendLine(st);
                    builder.AppendLine("GO"); // Se coloca GO al final de cada statement
                }

                // Escribiendo el SCRIPT
                script += builder.ToString();

                script += "--========================================================";
                script += "\r\nGO\r\n";
            }
            else
                script += "-- <WARNING> --" + "\r\n" + $"{schema}.{table}" + "NO SE PUDO ENCONTRAR" + "\r\n" + "-- </ WARNING >" + "\r\n";
                
            return script;
        }
    }
}
