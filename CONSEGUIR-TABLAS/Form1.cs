﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Smo.SqlEnum;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using System.Data.SqlClient;

namespace CONSEGUIR_TABLAS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            AgregarColumnas();
        }
        private void AgregarColumnas()
        {
            //Agregar columnas
            dataGridView1.Columns.Add("tablas", "Tablas");

            //AutoAjuste de Columnas
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }
        private void procedureButton_Click(object sender, EventArgs e)
        {
            string[] allLines = objetoTextBox.Text.Split('\n');

            foreach (string text in allLines)
            {
                if (string.IsNullOrEmpty(text))
                    continue;

                char[] MyChar = { '\r' };

                string NewString = text.TrimEnd(MyChar);

                var partes = NewString.Split('.').ToList();

                string dominio = partes.First();

                string tabla = partes.Last();

                string con = $"Data Source={ipTextBox.Text};Initial Catalog={comboBox1.Text};User ID={usuarioTextBox.Text};Password={passwordTextBox.Text};MultipleActiveResultSets=True";

                Server srv = new Server();
                srv.ConnectionContext.ConnectionString = con;

                Database db = new Database();

                db = srv.Databases[comboBox1.Text];
                Scripter scrp = new Scripter(srv);
                var urns = new List<Urn>();

                scrp.Options.EnforceScriptingOptions = true;
                scrp.Options.IncludeHeaders = true;
                scrp.Options.SchemaQualify = true;
                scrp.Options.SchemaQualifyForeignKeysReferences = true;
                scrp.Options.NoCollation = true;
                scrp.Options.DriAllConstraints = true;
                scrp.Options.DriAll = true;
                scrp.Options.DriAllKeys = true;
                scrp.Options.DriIndexes = true;
                scrp.Options.ClusteredIndexes = true;
                scrp.Options.NonClusteredIndexes = true;
                scrp.Options.ToFileOnly = true;

                Table table = db.Tables[tabla, dominio];
                StringCollection result = table.Script();

                foreach (var line in result)
                {
                    if (!table.IsSystemObject)
                    {
                        urns.Add(table.Urn);
                    }
                }

                // Instanciando un string builder para construir el script
                StringBuilder builder = new StringBuilder();
                System.Collections.Specialized.StringCollection sc = scrp.Script(urns.ToArray());

                foreach (string st in sc)
                {
                    // Agregando los comandos al string builder
                    builder.AppendLine(st);
                    builder.AppendLine("GO"); // Se coloca GO al final de cada statement
                }

                // Escribiendo el SCRIPT
                dataGridView1.Rows.Add(builder.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            objetoTextBox.Text = "";
        }
    }
}
