﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void procedureButton_Click(object sender, EventArgs e)
        {
            ScriptDB script = new ScriptDB();

            script.Nombre = objetoTextBox.Text;

            var procedureContentList = (new Repositorio()).GetProcedure(ipTextBox.Text, dbTextBox.Text, usuarioTextBox.Text, passwordTextBox.Text, objetoTextBox.Text);

            scriptTextBox.Text = (new ScriptDB()).GetText(procedureContentList);
        }

        
    }
}
