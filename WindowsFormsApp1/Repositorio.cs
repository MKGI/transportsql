﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Repositorio
    {
        public IEnumerable<string> GetProcedure(string ip, string baseDatos, string usuario, string password, string procedureName)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@objname", procedureName);

            using (IDbConnection db = new SqlConnection($"Data Source={ip};Initial Catalog={baseDatos};User ID={usuario};Password={password};MultipleActiveResultSets=True"))
            {
                return db.Query<string>("sp_helpText", parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
