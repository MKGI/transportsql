﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class ScriptDB
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public ScriptDB()
        {
        }
        public ScriptDB(string nombre)
        {
            this.nombre = nombre;
        }
        public string GetText(IEnumerable<string> procedureList)
        {
            
            string result = "";
            string basedatos = "VistoBuenoDB";
            string text = "USE" + " " + basedatos + "\r\n" + "GO" + "\r\n" + "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[RC].[" + this.nombre + "]') AND type in (N'P', N'PC'))" + "\r\n" + "BEGIN" + "\r\n" + "EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [RC].[" + this.nombre + "] AS'" + "\r\n" + "END" + "\r\n" + "GO" + "\r\n";

            foreach (var line in procedureList)
            {
                result += line;
            }

            return text + " " + result;
        }
    }
}
